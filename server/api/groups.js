let dbUtils = require('../utilities/dbUtils');
let boom = require('boom');

async function getMeetups (request, h) {

    let connection = await dbUtils.getConnection();

    try {
        let meetups = await  dbUtils.query(connection, `
            select m.meetupGroupId,
                   m.name,
                   m.url
            from devwi.meetupgroups m
            where m.isActive = 1;
        `);

        let meetupEvents = await  dbUtils.query(connection, `
            select e.meetupGroupId,
                   e.title,
                   e.description,
                   e.url as eventUrl,
                   e.start
            from devwi.meetupgroups m,
            devwi.events e
            where m.isActive = 1
            and e.id = (
                    select e2.id
                    from devwi.events e2
                    where e2.meetupGroupId = m.meetupGroupId
                      and e2.start > now()
                    order by e2.start
                    Limit 0, 1
           );
        `);

        let eventLookup = {};
        if (meetupEvents.value) {
            eventLookup = meetupEvents.value.reduce((accu, curValue) => {
                accu[curValue.meetupGroupId] = curValue;
                return accu;
            }, {});
        }

        let results = [];
        meetups.value.forEach((item) => {
            let event = eventLookup[item.meetupGroupId];
            results.push(Object.assign({}, item, {event: event}));
        });

        results.sort((a, b) => {
            if (a.name < b.name) {
                return -1;
            }
            if (a.name > b.name) {
                return 1;
            }
            return 0;
        });

        return results;

    }
    catch (e) {
        console.log(e);
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}

exports.plugin = {
    name: 'Meetups',
    version: '1.0.0',
    register: async (server, options) => {
        server.route([
            { method: 'GET',  path: '/groups', handler: getMeetups }
        ]);
    }
};










//
// const urls = ['https://www.meetup.com/milwaukeegraphicdesign/',
// 'https://www.meetup.com/Miltown-Game-Developers/',
// 'https://www.meetup.com/gener8tor/',
// 'https://www.meetup.com/Madison-Devops/',
// 'https://www.meetup.com/Ionic-Madison/',
// 'https://www.meetup.com/Milwaukee-Financial-Technology-Meetup/',
// 'https://www.meetup.com/MKEData/',
// 'https://www.meetup.com/Milwaukee-Lean-Entrepreneurs-Meetup/',
// 'https://www.meetup.com/MadisonIOT/',
// 'https://www.meetup.com/Milwaukee-Digital-Transformation-Meetup/'];
//
// let sleep = async (ms) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => resolve(), ms);
//     });
// };
//
// let getMeetupRequestUrl = (url) => {
//
//     let parsedUrl = nodeUrl.parse(url);
//     let getParms = querystring.stringify(
//         {
//             sign: true,
//             'photo-host': 'public',
//             key: '6441475e11d501d714b2b5023287148',
//             format: 'json'
//         }
//     );
//     return `https://api.meetup.com${parsedUrl.pathname}?` + getParms;
// };
//
// let getUserGroupInfo = async (url) => {
//
//     return new Promise((resolve, reject) => {
//         let groupUrl = getMeetupRequestUrl(url);
//         //console.log(groupUrl);
//         requestjs(groupUrl, (err, res, body) => {
//             if (err) return reject(err);
//             if (res.statusCode !== 200) return reject(new Error('Status Code:' + res.statusCode + ' ' + body));
//
//             if (body === undefined ||
//                 body === null ||
//                 body.length === 0)
//             {
//                 return resolve(null);
//             }
//
//             resolve(JSON.parse(body));
//
//         });
//     });
// };
//
//
// async function doSomething(){
//
//     for (let index = 0; index < urls.length; index++){
//         let connection = null;
//         let url = urls[index];
//
//         try {
//             await sleep(3000);
//             var groupInfo = await getUserGroupInfo(url);
//
//             connection = await dbUtils.getConnection();
//             await dbUtils.query(connection, 'INSERT INTO meetupgroups SET ?', {
//                 meetupGroupId: groupInfo.id,
//                 name: groupInfo.name,
//                 url: url,
//                 isActive: 1
//             });
//             console.log('done', url);
//
//         }
//         catch (e) {
//             console.log('error', e, url);
//         }
//         finally {
//             if (connection) connection.release();
//         }
//     }
// }
//
// doSomething();