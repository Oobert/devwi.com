var mysql = require('mysql');
var config = require('../config');
var connectionPool = mysql.createPool(config.db);

function getConnection () {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection( (err, connection) => {
            if (err) return reject(err);

            resolve(connection);
        });
    });
}

function query(connection, queryString, args) {

    return new Promise((resolve, reject) => {
        connection.query(queryString, args, (err, results, fields) => {
            if(err) return reject(err);
            resolve({connection: connection, value: results});
        });

    });
}



module.exports = { getConnection: getConnection, query: query };