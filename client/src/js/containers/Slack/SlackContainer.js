import React from 'react';
import PostComponent from '../../components/PostComponent';
let slackMd = require('./slack.md');

export default () => {
    let slack = {
        markdown: slackMd.default,
        publishedDate: ''
    };

    return (
        <div>
            <PostComponent post={slack}/>
        </div>
    );
}
