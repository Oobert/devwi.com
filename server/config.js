let fs = require('fs');
let path = require('path');

if (fs.existsSync(path.normalize(path.join(__dirname, '../../config.js')))) {
    module.exports = require('../../config');
} else {
    module.exports.db = {
        host: '127.0.0.1',
        user: 'root',
        password: 'password',
        database: 'devwi',
        multipleStatements: true,
        timezone: 'Z'
    };

    module.exports.http = {
        port: 3000
    };

    module.exports.secuirity = {
        password: 'password'
    };
}