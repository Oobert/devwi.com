'use strict';
let boom = require('boom');
let dbUtils = require('../utilities/dbUtils');
let RSS = require('rss');
let { Remarkable, utils } = require('remarkable');
let { linkify } = require('remarkable/linkify');
var hljs = require('highlight.js');

const md = new Remarkable('full', {
    html:         false,        // Enable HTML tags in source
    xhtmlOut:     false,        // Use '/' to close single tags (<br />)
    breaks:       false,        // Convert '\n' in paragraphs into <br>
    langPrefix:   'language-',  // CSS language prefix for fenced blocks
    linkTarget:   '',           // set target to open link in

    // Enable some language-neutral replacements + quotes beautification
    typographer:  true,

    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Set doubles to '«»' for Russian, '„“' for German.
    quotes: '“”‘’',

    // Highlighter function. Should return escaped HTML,
    // or '' if input not changed
    highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
            try {
                return hljs.highlight(lang, str).value;
            } catch (__) {}
        }

        try {
            return hljs.highlightAuto(str).value;
        } catch (__) {}

        return ''; // use external default escaping
    }
}).use(linkify);

async function getRss (request, h) {

    let feed = new RSS({
        title: 'DevWi LLC',
        description: 'DevWi Post Feed',
        feed_url: 'http://devwi.com/rss',
        site_url: 'http://devwi.com',
        //image_url: 'http://devwi.com/icon.png',

        managingEditor: 'Tony Gemoll',
        webMaster: 'Tony Gemoll',
        copyright: '2017 Tony Gemoll',
        language: 'en',
        ttl: '60'
    });

    let connection = null;

    try
    {
        connection = await dbUtils.getConnection();
        let results = await dbUtils.query(connection, `SELECT * 
                                                 FROM posts p
                                                 where p.published = 1
                                                 order by publishedDate
                                                 LIMIT 0, 20;`);

        results.value.forEach(item => {
            feed.item({
                title:  item.title,
                description: md.render(item.markdown),
                url: 'http://devwi.com/post/' + item.urlSafeTitle,
                guid: item.id,
                date: item.publishedDate, // any format that js Date can parse.
            });
        });

        return h.response(feed.xml()).type('application/xml');
    }
    catch (e)
    {
        console.log(e);
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}

async function getRssForTag (request, h) {

    let tag = request.params.tag ;

    let feed = new RSS({
        title: `DevWi LLC - Tag: ${tag}`,
        description: 'DevWi Post Feed',
        feed_url: 'http://devwi.com/rss',
        site_url: 'http://devwi.com',
        //image_url: 'http://devwi.com/icon.png',

        managingEditor: 'Tony Gemoll',
        webMaster: 'Tony Gemoll',
        copyright: '2017 Tony Gemoll',
        language: 'en',
        ttl: '60'
    });

    let connection = null;

    try
    {
        connection = await dbUtils.getConnection();
        let results = await dbUtils.query(connection, `SELECT p.*
                                                        FROM tags t
                                                        JOIN tag_post tp on t.id = tp.tag_id 
                                                        join posts p on tp.post_id = p.id
                                                        where t.slug = ?
                                                        and p.published = 1
                                                        order by publishedDate DESC
                                                    LIMIT 0, 20;`, [tag]);

        results.value.forEach(item => {
            feed.item({
                title:  item.title,
                description: md.render(item.markdown),
                url: 'http://devwi.com/post/' + item.urlSafeTitle,
                guid: item.id,
                date: item.publishedDate, // any format that js Date can parse.
            });
        });

        return h.response(feed.xml()).type('application/xml');
    }
    catch (e)
    {
        console.log(e);
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}


exports.plugin = {
    name: 'Rss',
    version: '1.0.0',
    register: async (server, options) => {
        server.route([
            { method: 'GET',  path: '/rss', handler: getRss },
            { method: 'GET',  path: '/rss/{tag}', handler: getRssForTag },
        ]);
    }
};
