'use strict';
let boom = require('boom');
let slug = require('slug');
let dbUtils = require('../utilities/dbUtils');
let config = require('../config');

async function getPostCount (request, h) {
    let connection = null;

    try
    {
        connection = await dbUtils.getConnection();
        let results = await dbUtils.query(connection, `SELECT count(id) as postCount
                                                        FROM posts p
                                                        where p.published = 1`);

        return results.value[0];
    }
    catch (e)
    {
        console.log(e);
        boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}

async function getPostCountForTag (request, h) {
    let tag = request.params.tag ;
    let connection = null;

    try
    {
        connection = await dbUtils.getConnection();
        let results = await dbUtils.query(connection, `SELECT count(p.id) as postCount
                                                        FROM tags t
                                                        JOIN tag_post tp on t.id = tp.tag_id 
                                                        join posts p on tp.post_id = p.id
                                                        where t.slug = ?
                                                        and p.published = 1`, [tag]);

        return results.value[0];
    }
    catch (e)
    {
        console.log(e);
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}

async function getPosts (request, h) {

    let pageNumber = request.params.page || 1;
    let skipRow = (pageNumber - 1) * 10;

    let connection = null;

    try
    {
        connection = await dbUtils.getConnection();
        let postsResult = await dbUtils.query(connection, `SELECT *
                                     FROM posts p
                                     where p.published = 1
                                     order by publishedDate DESC
                                     LIMIT ?, 10;`, [skipRow]);

        let posts = postsResult.value;
        await getSetTagsOnPosts(posts, connection);

        return posts;
    }
    catch (e)
    {
        console.log(e)
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}

async function getPostsForTag (request, h) {

    let pageNumber = request.params.page || 1;
    let tag = request.params.tag;
    let skipRow = (pageNumber - 1) * 10;

    let connection = null;

    try
    {
        connection = await dbUtils.getConnection();
        let postsResult = await dbUtils.query(connection, `SELECT p.*
                                                        FROM tags t
                                                        JOIN tag_post tp on t.id = tp.tag_id 
                                                        join posts p on tp.post_id = p.id
                                                        where t.slug = ?
                                                        and p.published = 1
                                                        order by publishedDate DESC
                                                    LIMIT ?, 10;`, [tag, skipRow]);

        let posts = postsResult.value;
        await getSetTagsOnPosts(posts, connection);

        return posts;
    }
    catch (e)
    {
        console.log(e);
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}

async function getPost (request, h) {

    let postTitle = request.params.postTitle;
    let connection = null;

    try
    {
        connection = await dbUtils.getConnection();
        let postsResult = await dbUtils.query(connection, `SELECT * 
                                                           FROM posts p
                                                           where p.urlSafeTitle = ?;`, [postTitle]);

        let posts = await postsResult.value;
        await getSetTagsOnPosts(posts, connection);

        return posts[0];
    }
    catch (e)
    {
        console.log(e);
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }
}

// function savePost (request, h){
//
//     if (request.payload.password !== config.secuirity.password){
//         reply(boom.forbidden("Nope"));
//         return;
//     }
//
//     dbUtils.getConnection()
//         .then((connection) => dbUtils.query(connection, 'INSERT INTO posts SET ?', {
//             title: request.payload.title,
//             markdown: request.payload.markdown,
//             urlSafeTitle: slug(request.payload.title),
//             published: 1,
//             publishedDate: new Date()
//         }))
//         .then((queryResults) => {
//             reply();
//         })
//         .catch((err) => {
//             reply(boom.badImplementation(new Error(err)));
//         });
//
// }

async function getSetTagsOnPosts(posts, connection) {
    let postIds = posts.map((item) => {
        return item.id;
    });

    let tagsResult = await dbUtils.query(connection, `select tp.post_id, t.name, t.slug
                                                        from tag_post tp
                                                        join tags t on tp.tag_id = t.id
                                                        where tp.post_id in (?);`, [postIds]);
    let tags = tagsResult.value;

    posts.forEach((post) => {
        let tagsForPost = tags.filter((tag) => {
            return tag.post_id === post.id;
        });
        post.tags = tagsForPost || [];
    });
}

exports.plugin = {
    name: 'Posts',
    version: '1.0.0',
    register: async (server, options) => {
        server.route([
            { method: 'GET',  path: '/post/{postTitle?}', handler: getPost },
            { method: 'GET',  path: '/posts/{page?}', handler: getPosts },
            { method: 'GET',  path: '/{tag}/posts/{page?}', handler: getPostsForTag },
            { method: 'GET',  path: '/posts/count', handler: getPostCount },
            { method: 'GET',  path: '/{tag}/posts/count', handler: getPostCountForTag },
           // { method: 'POST', path: '/post', handler: savePost}
        ]);
    }
};
