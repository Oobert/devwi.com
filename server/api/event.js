'use strict';
let boom = require('boom');
let dbUtils = require('../utilities/dbUtils');
let moment = require('moment');

async function getEvents (request, h) {

    let start = new Date(request.query.start);
    let end = new Date(request.query.end);
    let connection = null;
    try
    {
        connection = await dbUtils.getConnection();
        let queryResults = await dbUtils.query(connection,
            'SELECT * FROM events e where e.start >= ? and e.start <= ?;',
            [start, end]);

        let events = [];
        queryResults.value.forEach((item) => {
            events.push({
                id: item.id,
                title: item.name + ' - ' + item.title,
                allDay: false,
                start: item.start,
                end: item.end,
                url: item.url,
                description: item.description
            });
        });
        return events;
    }
    catch (e)
    {
        console.log(e);
        return boom.badImplementation(new Error(e));
    }
    finally {
        if (connection) connection.release();
    }


}

 function submitMeetup(request, reply) {
//
//     connectionPool.getConnection( (err, connection) => {
//         if (err){
//             console.log(err);
//             return reply(boom.badImplementation(new Error('Meetup Request failed - NO DB Connection')));
//         }
//
//
//
//     });
 }


exports.plugin = {
    name: 'Events',
    version: '1.0.0',
    register: async (server, options) => {
        server.route([
            { method: 'GET',  path: '/events', handler: getEvents },
            //{ method: 'POST', path: '/event/meetup', handler: submitMeetup}
        ]);
    }
};