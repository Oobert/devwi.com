let Hapi = require('@hapi/hapi');
let boom = require('boom');
let config = require('./config');


const server = new Hapi.Server({
    host: '0.0.0.0',
    port: config.http.port
})


const routeAndStart = async (err) => {
    if (err) return console.log(err);

    await server.register(require('@hapi/inert'));
    await server.register({ plugin: require('./api/posts'), routes: { prefix: '/api' } });
    await server.register({ plugin: require('./api/event'), routes: { prefix: '/api' } });
    await server.register({ plugin: require('./api/groups'), routes: { prefix: '/api' } });
    await server.register({ plugin: require('./api/rss')});

    server.route([
        {
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory: {
                    index: true,
                    path: 'dist'
                }
            }
        }
    ]);

    server.ext('onPreResponse', (request, h) => {
        const response = request.response;
        if (response.isBoom && response.output.statusCode === 404) {
            return h.file('dist/index.html');
        }
        return h.continue;
    });

    await server.start();

    console.log('Server running at:', server.info.uri);
};

routeAndStart();