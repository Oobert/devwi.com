import React, { useReducer, useRef, useLayoutEffect } from 'react';
import {Form, Button, TextArea, Tab, Input} from "semantic-ui-react";
import md from '../modules/markdown';

const initState = {
    eventTitle: '',
    eventUrl: '',
    eventStart: '',
    eventEnd: '',
    eventDescription: ''
};
const reducer = (state, action) => {
    switch (action.type) {
        case 'formUpdate':
            let updatedState = {...state};
            updatedState[action.payload.name] = action.payload.value;
            return updatedState;
        default:
            return state;
    }
};


export default () => {
    const initFocus = useRef(null);
    const [state, dispatch] = useReducer(reducer, initState);
    useLayoutEffect(() => {
        initFocus.current.focus()
    }, [true]);


console.log(state);
    const panes = [
        { menuItem: 'Text', render: () => {
            return (
                <Tab.Pane>
                    <TextArea
                        name='eventDescription'
                        value={state.eventDescription}
                        onChange={(e) => dispatch({type: 'formUpdate', payload: e.target})}
                    />
                </Tab.Pane>
            );
            }
        },
        { menuItem: 'Preview', render: () => {
            return (
                <Tab.Pane>
                    <div dangerouslySetInnerHTML={{__html: md.render(state.eventDescription)}} />
                </Tab.Pane>
            );
            }
        }
    ];

    return (
        <div>
            <Form onSubmit={() => {}}>
                <Form.Field>
                    <label>Event Title:</label>
                    <Input
                        ref={initFocus}
                        type='text'
                        name='eventTitle'
                        value={state.eventTitle}
                        onChange={(e) => dispatch({type: 'formUpdate', payload: e.target})}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Event Url:</label>
                    <Input
                        type='url'
                        name='eventUrl'
                        value={state.eventUrl}
                        onChange={(e) => dispatch({type: 'formUpdate', payload: e.target})}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Event Start:</label>
                    <Input
                        type='datetime-local'
                        name='eventStart'
                        value={state.eventStart}
                        onChange={(e) => dispatch({type: 'formUpdate', payload: e.target})}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Event End:</label>
                    <Input
                        type='datetime-local'
                        name='eventEnd'
                        value={state.eventEnd}
                        onChange={(e) => dispatch({type: 'formUpdate', payload: e.target})}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Event Description (accepts markdown):</label>
                    <Tab panes={panes} />
                </Form.Field>

                <Button type='submit'>Send to calender!</Button>
            </Form>
        </div>
    )
}