FROM node:12.16.1

# Create app directory
WORKDIR /usr/src/devwi

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

RUN npm run web:build:prod

EXPOSE 3000
CMD [ "node", "server/app.js" ]