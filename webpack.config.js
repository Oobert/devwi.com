const webpack = require('webpack');
const path = require('path');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// used to inject the bundle directly into index.html
const HtmlWebpackPlugin = require('html-webpack-plugin');

// used to copy files to the dist folder. In this case the data folder
const CopyWebpackPlugin = require('copy-webpack-plugin');

// cleans out the dist folder on every build
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// Force writes dist folder when using webpack-dev-server
// This is need to that the webpack dev server will serve the data
// folder other wise it wont because it is not part of the js bundle.
const WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    entry: {
        main: './client/src/js/app.js'
    },
    output: {
        filename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, 'dist')
    },

    devtool: 'source-map',

    module: {
        rules: [
            {test: /\.html$/, loader: 'html-loader'},
            {test: /\.css$/, use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader"
                ]
            },
            {test: /\.(png|jpg|jpeg|svg|eot|woff2|woff|ttf)$/, loader: 'file-loader'},
            {test: /\.(md)$/, loader: 'raw-loader'},
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ],
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    chunks: "initial",
                    name: "vendor",
                    priority: 10,
                    enforce: true
                }
            }
        }
    },
    plugins:[
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin([
            { from: 'client/src/fav' },
        ]),
        new HtmlWebpackPlugin({
            template: 'client/index.html',
            inject: 'body'
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new WriteFilePlugin()
    ]
};
